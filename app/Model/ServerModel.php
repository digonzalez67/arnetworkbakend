<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServerModel extends Model
{
    protected $table = "servers";

    public function findVuforia ($id) {
        return $this->where("target_vuforia",$id)->first();
    }

}
