<?php

namespace App\Http\Controllers;

use App\Model\ServerModel;

class DiscController extends Controller
{
    private $serverModel;
    private $unit;
    private $size;
    private $used;
    /**
     * CpuController constructor.
     */
    function __construct(ServerModel $serverModel)
    {
        $this->serverModel = $serverModel;
        $this->unit = ".25.2.3.1.4.3";
        $this->size = ".25.2.3.1.5.3";
        $this->used = ".25.2.3.1.6.3";
    }

    function getInfo () {
        $snmp =new SNMPController();
        $snmp->setIp($this->serverModel->ip_server);
        $snmp->setOid($this->serverModel->oid);

        $unit = $snmp->getSnmpWalk($this->unit);
        $size = $snmp->getSnmpWalk($this->size);
        $used = $snmp->getSnmpWalk($this->used);
        return
            (object) [
                "total" => ($size * $unit) / 1024,
                "used" => ($used * $unit) / 1024,
                "percentage" => ( ($used / $size) * 100 )
            ];
    }
}
