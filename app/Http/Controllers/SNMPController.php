<?php

namespace App\Http\Controllers;

class SNMPController extends Controller
{
    private $ip;
    private $oid;
    public function setIp ($ip) {
        $this->ip = $ip;
    }
    public function setOid ($oid) {
        $this->oid = $oid;
    }
    public function getIp () {
        return $this->ip;
    }
    public function getOid () {
        return $this->oid;
    }

    public function getSnmpWalk ($oid_complement , $oidComplete = false ) {
        return $this->Parse(snmpwalk($this->ip,"mysite",( $oidComplete ) ? $oid_complement : $this->oid.$oid_complement));
    }
    public function getSnmpGet ($oid_complement) {
        return $this->Parse(snmpget($this->ip,"mysite",$this->oid.$oid_complement));
    }

    public function Parse ($response) {
        return explode( " ", trim( explode(":", $response[0])[1])) [0];
    }
}
