<?php

namespace App\Http\Controllers;

use App\Model\ServerModel;

class CpuController extends Controller
{
    private $serverModel;
    private $used;
    /**
     * CpuController constructor.
     */
    function __construct(ServerModel $serverModel)
    {
        $this->serverModel = $serverModel;
        // snmpwalk.exe -v 2c -c mysite 192.168.1.3 .1.3.6.1.4.1.2021.11.9
        $this->used = ".1.3.6.1.4.1.2021.11.9";
    }

    function getInfo () {
        $snmp =new SNMPController();
        $snmp->setIp($this->serverModel->ip_server);
        $snmp->setOid($this->serverModel->oid);

        $used = $snmp->getSnmpWalk($this->used , true);

        return
            (object) [
                "used" => (int)$used
            ];
    }
}
